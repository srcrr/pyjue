import os
import click
import sys
import requests
from requests.auth import HTTPBasicAuth
from dotenv import load_dotenv
import re
import jinja2
from .tags import now
import subprocess
import typing as T

load_dotenv()

URL_JIRA_TICKET_DATA = (
    "https://teamworx-security.atlassian.net/rest/api/3/issue/{ticket_name}".format
)

# Set up the jinja environment
env = jinja2.Environment()
env.filters["now"] = now


def get_auth(require=True) -> T.Tuple:
    auth = (os.getenv("JIRA_USERNAME"), os.getenv("JIRA_PASSWORD"))
    if require and None in auth:
        raise RuntimeError(
            "Please set `JIRA_USERNAME` and `JIRA_PASSWORD` in the `.env` file or as an environment variable."
        )
    return auth


def get_branch_format() -> T.AnyStr:
    branch_format = os.getenv("BRANCH_FORMAT")
    if not branch_format:
        raise RuntimeError("Expected BRANCH_FORMAT to be set.")
    return branch_format


def formatted_branch(ticket_name: T.AnyStr, ticket_summary: T.AnyStr) -> T.AnyStr:
    branch_format = get_branch_format()
    context = {
        "ticket": ticket_name,
        "slug": machine(ticket_summary),
    }
    return env.from_string(branch_format).render(**context)


def fetch_ticket_data(ticket_name: T.AnyStr) -> T.Dict:
    resp = requests.get(URL_JIRA_TICKET_DATA(ticket_name=ticket_name), auth=get_auth())
    if resp.status_code != 200 and "errorMessages" in resp.json():
        err_msgs = "\n".join(resp.json()["errorMessages"])
        raise RuntimeError(err_msgs, err=True)
    return resp.json()


def fetch_ticket_summary(ticket_name: T.AnyStr) -> T.AnyStr:
    ticket_data = fetch_ticket_data(ticket_name)
    return ticket_data["fields"]["summary"]


def machine(sentence: T.AnyStr) -> T.AnyStr:
    return re.sub("[^\w]+", "-", sentence.lower()).strip("-").rstrip("-")


def construct_git_command(branch_name: T.AnyStr) -> T.Iterable[T.AnyStr]:
    return ["git", "checkout", "-b", branch_name]


@click.command()
@click.argument("ticket_name")
@click.option(
    "--dry-run", "-d", is_flag=True, help="Perform a dry run to test branch name."
)
def new_ticket(ticket_name: T.AnyStr, dry_run=False):
    summary = fetch_ticket_summary(ticket_name)
    bname = formatted_branch(ticket_name, summary)
    cmd = construct_git_command(bname)
    if dry_run:
        click.echo(" ".join(cmd))
    else:
        subprocess.call(cmd)


if __name__ == "__main__":
    new_ticket()  # pylint: disable=no-value-for-parameter
