from datetime import datetime

def now(date_format : str):
    return datetime.now().strftime(date_format)