import unittest
import re
from .tags import now
from .cli import (
    get_prefix,
    fetch_ticket_summary,
    fetch_ticket_data,
    machine,
    branch_name,
)

class TestTags(unittest.TestCase):

    def test_now(self):
        expected = re.compile(r'The week is \d+').match
        result = now("The week is %W")
        self.assertTrue(expected(result))

        expected = re.compile(r'\d+ of week \d+').match
        result = now("%Y of week %W")
        self.assertTrue(expected(result))

class TestStringUtils(unittest.TestCase):

    def test_machine(self):
        expected = "this-is-a-machine-name-without-symbols-or-spaces"
        result = machine("This is ..... a MACHINE|name     without\n symbols or spaces.")
        self.assertEqual(result, expected)