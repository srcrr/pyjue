# Project Title

One Paragraph of project description goes here

This initially appeared on
[gist](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2), but as
I can no longer open that page as there are too many comments, I have
moved it here.

## Summary

- [Getting Started](#getting-started)
- [Runing the tests](#running-the-tests)
- [Deployment](#deployment)
- [Built With](#built-with)
- [Contributing](#contributing)
- [Versioning](#versioning)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)

## Getting Started

These instructions will get you a copy of the project up and running on
your local machine for development and testing purposes. See deployment
for notes on how to deploy the project on a live system.

### Prerequisites

- Python 3.8
- Poetry (recommended)

### Installing

Clone or download the package.

Using Poetry

```
$ poetry install
```

Without poetry

```
$ pip install .
```

### Usage

After installation a program should be available:

```
$ jbranch --help
Usage: jbranch [OPTIONS] TICKET_NAME

Options:
  --help  Show this message and exit
```

For example:

```
$ jbranch TK-002
Switched to a new branch 'my-prefix-march-21-2021_TK-002_some-ticket-summary-goes-here'
```

### Environment variables

You can either set the environment variables in `.env` or set them in the terminal.

```bash
# Jinja2-style ticket argument
# There is a custom filter `now` which formats the current date/time
# given the strftime string
# In addition `ticket`, and `slug` are variables also available.
BRANCH_FORMAT='jirausername-{{ "%Y%B%d"|now }}_{{ ticket }}_{{ slug }}'

# IMPORTANT: Jira credentials
JIRA_USERNAME='my-jira-username'
JIRA_PASSWORD='my-jira-password'
```

## Running the tests

Several of the tests require the environment variables to be set in
order to pass.

```
$ python -m unittest discover
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions
available, see the [tags on this
repository](https://github.com/PurpleBooth/a-good-readme-template/tags).

## Authors

- **Jordan Hewitt** - _Original Author_
  [TheSrcRr]()

- **Billie Thompson** - _Provided README Template_ -
  [PurpleBooth](https://github.com/PurpleBooth)

See also the list of
[contributors](https://github.com/PurpleBooth/a-good-readme-template/contributors)
who participated in this project.

## License

This project is licensed under the [GPLv3](LICENSE.md)License - see the [LICENSE.md]
(LICENSE.md) file for details

## Acknowledgments

- Open to suggestions :)